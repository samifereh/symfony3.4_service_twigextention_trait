-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 19 Octobre 2018 à 13:19
-- Version du serveur :  10.0.36-MariaDB-0ubuntu0.16.04.1
-- Version de PHP :  7.1.23-2+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `myproject`
--

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sexe` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `sexe`, `email`, `password`) VALUES
(1, 'Sami', 'FAREH', 1, 'samiferah@tritux.com', '$2y$12$TmuQhbty9XWxHTinfRI9L.k0.nqjgl/gUDpjuenntdyofIFCyTbmm'),
(2, 'Julie', 'Antoinette', 0, 'ef.ghi@test.local', '$2y$12$efNHi4jz4M8WSyqFiBeiS.phhTIQli3XTdB7HXvhnzmbnkfd.QtKO'),
(3, 'Dupont', 'Sandrine', 0, 'JeanDupont@gmail.com', '$2y$12$rjagDueSrQ.jAAfiHLo.2e3LugMC/h1X6i54m1ikMHIBdB3jNRyG6'),
(4, 'Aurélie', 'Aline', 1, 'ef.ghik@test.local', '$2y$12$C2lASujVPy2XXrLtCeLxI./Px0E6w.HxxuDIpJDtlN9qQIdvAF36u'),
(5, 'Bensalem', 'Aymen', 1, 'ef.ghC@test.local', '$2y$12$HLmag81S38FN1.cqr1rtMuXvkAz7IdJgO.HpDsZ58DZaeKSu3DmSC');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
