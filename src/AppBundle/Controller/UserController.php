<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User;
use AppBundle\Service\UserService;
use Symfony\Component\HttpFoundation\StreamedResponse;

class UserController extends Controller
{
    /**
     * @Route("/users/export", name="users_export")
     */
    public function ExportUsersAction(Request $request)
    {
        $id = $request->get('id');
        $serviceTrait = $this->get('app.service_trait');
        $return = $serviceTrait->exportCsv($id);
        return $return;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/users", name="users_list")
     * @Method({"GET"})
     */
    public function UserListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('AppBundle:user:index.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/users/{id}", name="user_one")
     * @Method({"GET"})
     */
    public function ShowUserAction(Request $request, $id)
    {
        $serviceTrait = $this->get('app.service_trait');
        $user = $serviceTrait->getUserById($id);
        return $this->render('AppBundle:user:show.html.twig', ['user' => $user]);
    }

    /**
     * @Route("/users_trait/{id}", name="user_one_trait")
     * @Method({"GET"})
     */
    public function ShowUserTraitAction(Request $request, $id)
    {
        $serviceTrait = $this->get('app.service_trait');
        die($serviceTrait->getPseudoByUser($id));
        return $this->render('AppBundle:user:showTrait.html.twig', ['user' => $user]);
    }



}
