<?php
namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sensio\Bundle\GeneratorBundle\Command\Helper\DialogHelper;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

class  ExportCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('export:users')

            // the short description shown while running "php bin/console list"
            ->setDescription('Export list of users.')

            ->addArgument('file', InputArgument::OPTIONAL, 'The path for the file.')
            ->addOption('path')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to export the list of users...')
        ;

    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {

    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $csvDefaultPath = 'web/uploads';
        $dateExt = date("d_m_Y").'.csv';
        $csvDefaultFileName = 'users_'. date("d_m_Y").'.csv';

        $DefaultScript = ($input->getArgument('file')=='')?'Default':'Customized';

        if ($input->getArgument('file')!='' &&
            !file_exists($input->getArgument('file')) && 
            !is_dir($input->getArgument('file')))

            die('Path does not existing, check it please');

        else if($input->getArgument('file')==''){
            $output->writeln('Your file has been saved in: '.$csvDefaultPath);
            $output->writeln('Your file name is: '.$csvDefaultFileName);
            if($csvDefaultFileName!=''){
            $exportService = $this->getContainer()->get('app.service_trait');
            $csvPath = $csvDefaultPath.'/'.$csvDefaultFileName;
            $return = $exportService->exportUsersList($csvPath);
        }
        die;

        }else{

        $helper = $this->getHelper('question');
        //$question = new Question('Please enter the path name   ', $csvDefaultPath);
        $question1 = new Question('Please enter the name of the file   ', 'users');
        
        //$path = $helper->ask($input, $output, $question);
        $path = $input->getArgument('file');
        $file_name = $helper->ask($input, $output, $question1).'_'.date("d_m_Y").'.csv';

        //$output->writeln('Argument: '.$input->getArgument('file'));
        $output->writeln('Your file has been saved in: '.$path);
        $output->writeln('Your file name is: '.$file_name);

        if($path!=''){
            $exportService = $this->getContainer()->get('app.service_trait');
            $csvPath = $path.'/'.$file_name;
            $return = $exportService->exportUsersList($csvPath);
        }

        }

        }
}