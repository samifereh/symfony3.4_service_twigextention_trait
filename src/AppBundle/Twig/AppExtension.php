<?php

namespace AppBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\Extension\RuntimeExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerInterface;


class AppExtension  extends \Twig_Extension
{

    private $userservice;

    public function __construct($userservice=null)
    {
        $this->userservice = $userservice;
    }

    public function getFilters()
    {
        return array(
            // the logic of this filter is now implemented in a different class
            new TwigFilter('civ' , array($this, 'sexeFilter')),
        );
    }

	public function getFunctions(){
  	 	return array(
      	'pseudo' => new \Twig_Function_Method($this, 'pseudo'),
  	 );
	}

    //fonction utilisé dans le cas d'utilisation d'un filtre
    public function sexeFilter($user)
    {
        $sexe = $user->getSexe();
        $fullName = $user->getLastname().' '.$user->getFirstname();
        $civ = ($sexe==0) ? 'Mme/Mlle '.$fullName : 'Mr. '.$fullName;
        return $civ;
    }
 
	public function pseudo($user){
	  	$pseudo = $this->userservice->getPseudoByUser($user);
		return $pseudo;
	}
}