<?php
namespace AppBundle\Traits;

trait TraitTools
{
 
    protected function getDoctrine()
    {
         
        return $this->container->get('doctrine');
    }

     
    protected function getRepository()
    {
         
        return $this->getDoctrine()->getRepository($this->entityClassName);
    }

}