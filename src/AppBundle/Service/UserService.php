<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class UserService
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * UserService constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getUserById($id) {
        $em = $this->entityManager;
        $user =  $em->getRepository('AppBundle:User')->findOneBy(['id' => $id]);
        return $user;
    }

    public function getPseudoByUser($user) {
        $sexe = $user->getSexe();
        $civ = ($sexe=='0')?'Mme/Mlle':'Mr.';   
        $pseudo = $civ.' '.$user->getLastname().' '.$user->getFirstname();
        return $pseudo;
    }

}