<?php

namespace AppBundle\Service;

use AppBundle\AppBundle;
use AppBundle\Traits\TraitTools;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerAwareTrait as CAT;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ServiceTrait
{
    use 
    CAT,
    TraitTools;

    protected $entityClassName = 'AppBundle\Entity\User';
    
    public function getUserById($id) {
        $user = $this->getRepository('AppBundle:User')->findOneBy(['id' => $id]);
        return $user;
    }

    public function getPseudoByUser($id) {
        $user=$this->getUserById($id);
        $sexe = $user->getSexe();
        $civ = ($sexe=='0')?'Mme/Mlle':'Mr.';   
        $pseudo = $civ.' '.$user->getLastname().' '.$user->getFirstname();
        return $pseudo;
    }

    //Impléméntation de l'Extract dans une seule fonction
    public function exportCsv($id=null)
    {
        $users=$id!=null
        ?$this->getRepository(User::class)->findById($id)
        :$this->getRepository(User::class)->findAll();

        if(!$users){
            die('No users found');
        }

        $fileName = "export_users_" . date("d_m_Y") . ".csv";
        $response = new StreamedResponse();      

        $response->setCallback(function() use ($users){
        $handle = fopen('php://output', 'w+');

        // Nom des colonnes du CSV
        fputcsv($handle, array('Nom','Prénom' ,'Civilité', 'Email'), ';');

        //Champs
      
        foreach ($users as $index => $user)
        {

            $sexe = $user->getSexe();
            $civ = ($sexe=='0')?'Mme/Mlle':'Mr.';
            fputcsv($handle,array(
                $user->getLastname(),
                $user->getFirstname(),
                $civ,
                $user->getEmail(),
            ),';');
        }

        fclose($handle);
    });

    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/csv; charset=utf-8', 'application/force-download');
    $response->headers->set('Content-Disposition','attachment; filename='.$fileName);

        return $response;
         
    }

    public function exportUsersList($csvPath='')
    {

        $fileName = "export_users_" . date("d_m_Y") . ".csv";
        if($csvPath != "")
            return $this->exportFile($csvPath);

        $response = new StreamedResponse();

        $response->setCallback(function() {

            $this->exportFile('php://output');
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition','attachment; filename="export-users.csv"'.$fileName);

        return $response;
    }

    protected function exportFile($filePath)
    {
        $handle = fopen($filePath, 'w+');
        fputcsv($handle, ['Nom', 'Prénom', 'Civilité', 'Email'], ';');

        $results = $this->getRepository('AppBundle:User')->findAll();
        foreach ($results as $user) {
            $sexe = $user->getSexe();
            $civ = ($sexe=='0')?'Mme/Mlle':'Mr.';
            fputcsv(
                $handle,
                [$user->getLastname(), $user->getFirstname(), $civ, $user->getEmail()],
                ';'
            );
        }

        fclose($handle);
    }

    }